package ds13.bots.scripts

import ds13.bots.AbstractBotDescriptor
import ds13.bots.AbstractBotHelper
import ds13.bots.BotController
import ds13.bots.Start
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessWithPause
import ds13.bots.GetStatus
import org.json.JSONObject
import ds13.logger.Logger
import ds13.logger.PrintAllLogger

object TestBotObj {

  val NAME = "Test bot v1"

}

class TestBotConfig1 extends TraitBotConfig

class TestBotDescriptor1(val logger: Logger) extends AbstractBotDescriptor[TestBotResult1, TestBotProcessConfig1, TestBotConfig1, TestBotProcess1, TestBotHelper1](TestBotObj.NAME) {

  override def newConfig: TestBotConfig1 = new TestBotConfig1

  override def createHelper(config: TestBotConfig1): Option[TestBotHelper1] =
    Some(new TestBotHelper1(logger, config))

}

class TestBotResult1

class TestBotProcessConfig1

class TestBotProcess1(override val logger: Logger, override val id: Long) extends TraitBotProcessWithPause[TestBotResult1, TestBotProcessConfig1] {

  override def step = println("Bot works")

  override def pause = 2000

  override def toJSON = None

}

class TestBotHelper1(logger: Logger, config: TestBotConfig1)
    extends AbstractBotHelper[TestBotResult1, TestBotProcessConfig1, TestBotConfig1, TestBotProcess1](logger, config) {

  override protected def createBotProcess(config: TestBotConfig1, id: Long): Option[TestBotProcess1] =
    Some(new TestBotProcess1(logger, id))
}

class TestBotContoller extends BotController[TestBotResult1, TestBotProcessConfig1, TestBotConfig1, TestBotProcess1, TestBotHelper1, TestBotDescriptor1]("test1") {

}

object ExecutorTest1 extends App {

  val logger = PrintAllLogger()

  val controller = new TestBotContoller

  val testBotDescriptor = new TestBotDescriptor1(logger)

  controller.addDescriptor(testBotDescriptor)
  controller.createBot(TestBotObj.NAME, 1) foreach { id =>
    controller.sendMsg(id, "start")
    controller.getBotHelper(id) foreach { helper =>
      println(helper.status)
    }
    controller.sendMsg(id, "stop")
    controller.getBotHelper(id) foreach { helper =>
      println(helper.status)
    }
    Thread.sleep(10000)
    controller.getBotHelper(id) foreach { helper =>
      println(helper.status)
    }
  }

}