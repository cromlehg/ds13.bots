package ds13.bots

trait TraitBotDescriptor[+R, +C, F <: TraitBotConfig, P <: TraitBotProcess[R, C], H <: TraitBotHelper[R, C, F, P]] {

  val name: String

  def newConfig: F

  def createHelper(config: F): Option[H]

}

abstract class AbstractBotDescriptor[+R, +C, F <: TraitBotConfig, P <: TraitBotProcess[R, C], H <: TraitBotHelper[R, C, F, P]](override val name: String) extends TraitBotDescriptor[R, C, F, P, H]