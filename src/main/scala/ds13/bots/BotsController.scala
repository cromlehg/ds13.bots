package ds13.bots

import java.util.concurrent.atomic.AtomicLong

import CommmonImplicits.appContext
import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.reflect.ClassTag
import scala.util.Failure
import scala.util.Success

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout

class BotController[R, C, F <: TraitBotConfig: ClassTag, P <: TraitBotProcess[R, C], H <: TraitBotHelper[R, C, F, P]: ClassTag, D <: TraitBotDescriptor[R, C, F, P, H]](
    val name: String /*, startCounter: Long = 0*/ ) {

  //val unqueIdStorage = new AtomicLong(startCounter + 1)

  private val system = ActorSystem(name)

  private val log = system.log

  private val descrs = new java.util.concurrent.ConcurrentHashMap[String, D]()

  private val bots = new java.util.concurrent.ConcurrentHashMap[Long, H]()

  def getDescriptor(name: String): Option[D] = if (descrs.get(name) == null) None else Some(descrs.get(name))

  def getBotHelper(id: Long): Option[H] = {
    val botHelper = bots.get(id)
    if (botHelper == null) None else Some(botHelper)
  }

  def addDescriptor(descr: D): D = descrs.put(descr.name, descr)

  def descrsCount(filter: (String, String)*): Long = descrs.size

  def descrsPagesCount(size: Int, filter: (String, String)*): Long = {
    val rowCount = descrsCount(filter: _*)
    if (rowCount % size > 0) rowCount / size + 1 else rowCount / size
  }

  def descrsPage(size: Int, pageNumber: Int, filter: (String, String)*): List[D] = {
    val beforeItems = if (pageNumber > 0) (pageNumber - 1) * size else 0
    val afterItems = beforeItems + size + 1
    descrs.values.toList.slice(
      if (beforeItems > descrs.size()) descrs.size() else beforeItems,
      if (afterItems > descrs.size()) descrs.size() else afterItems)
  }

  def botsCount(filter: (String, String)*): Long = bots.size

  def botsPagesCount(size: Int, filter: (String, String)*): Long = {
    val rowCount = botsCount(filter: _*)
    if (rowCount % size > 0) rowCount / size + 1 else rowCount / size
  }

  def botsPage(size: Int, pageNumber: Int, filter: (String, String)*): List[H] = {
    val beforeItems = if (pageNumber > 0) (pageNumber - 1) * size else 0
    val afterItems = beforeItems + size + 1
    bots.values.toList.slice(
      if (beforeItems > bots.size()) bots.size() else beforeItems,
      if (afterItems > bots.size()) bots.size() else afterItems)
  }

  private def withBotHelperBool(botId: Long)(f: H => Boolean): Boolean =
    bots.get(botId) match {
      case botHelper: H => f(botHelper)
      case _            => false
    }

  private def withActorRefBool(botId: Long)(f: ActorRef => Boolean): Boolean =
    withBotHelperBool(botId)(_.ref match {
      case Some(ref) => f(ref)
      case _         => false
    })

  private def withBotHelper[T](botId: Long)(f: H => Option[T]): Option[T] =
    bots.get(botId) match {
      case botHelper: H => f(botHelper)
      case _            => None
    }

  private def withActorRef[T](botId: Long)(f: ActorRef => Option[T]): Option[T] =
    withBotHelper(botId)(_.ref flatMap f)

  def remove(botId: Long): Option[ActorRef] =
    withActorRef(botId) { ref =>
      ref ! Remove
      bots.remove(botId)
      Some(ref)
    }

  def sendMsg(botId: Long, msg: ControlMessage): Boolean =
    withActorRefBool(botId) { ref =>
      msg match {
        case Remove() => remove(botId).fold(false)(t => true)
        case otherMsg => {
          log.debug("ActorBotSystem.sendMsg(" + botId + ", " + msg.toString + "): sending message to actor...")
          ref ! otherMsg
          log.debug("ActorBotSystem.sendMsg(" + botId + ", " + msg.toString + "): message has been sent to actor")
          true
        }
      }
    }

  def sendReq(botId: Long, msg: ControlMessage): Option[ResultMessage] =
    withActorRef(botId) { ref =>
      implicit val timeout = Timeout(10 seconds)
      val future = ref ? GetCurrentResult
      future onComplete {
        case Success(result) => Some(result)
        case Failure(error)  => None
      }
      try {
        (Await result (future, timeout.duration)).asInstanceOf[Option[ResultMessage]]
      } catch {
        case ex: Exception => None
      }
    }

  private def createBotByConfig(descr: D, config: F, uid: Long)(f: F => Boolean): Option[Long] =
    if (f(config)) {
      descr.createHelper(config) match {
        case Some(botHelper) =>
          //val uid = unqueIdStorage.getAndIncrement
          botHelper.createActor(uid) match {
            case Some(actorProps) =>
              val ref = system.actorOf(actorProps, name = uid.toString)
              if (ref == null) {
                log.debug("Couldn't create new actor ref in actor system " + descr.name + " with config " + config.getClass.getSimpleName + "!")
                None
              } else {
                botHelper.ref = Some(ref)
                bots.put(uid, botHelper)
                log.debug("New actor reference has been created with description " + descr.name + " with config " + config.getClass.getSimpleName + "!")
                Some(uid)
              }
            case _ =>
              log.debug("Couldn't create actor for bot " + descr.name + " with config " + config.getClass.getSimpleName + "!")
              None
          }
        case _ =>
          log.debug("Couldn't create bot helper for bot " + descr.name + " with config " + config.getClass.getSimpleName + "!")
          None
      }
    } else {
      log.debug("Bot creation for " + descr.name + " broken because cofiguration function for " + config.getClass.getSimpleName + " return false!")
      None
    }

  def createBot(descriptorName: String, id: Long): Option[Long] =
    createConfiguredBot(descriptorName, id) { _: TraitBotConfig => true }

  def createConfiguredBot(descriptorName: String, id: Long)(f: F => Boolean): Option[Long] = {
    getDescriptor(descriptorName) flatMap { descr =>
      descr.newConfig match {
        case castedConfig: F =>
          createBotByConfig(descr, castedConfig, id)(f)
        case config =>
          log.debug("Couldn't cast bot config " + config.getClass.getSimpleName + " for bot descriptor " + descr.name + "!")
          None
      }

    }
  }

  def stringMsgToCommand(msg: String): Option[ControlMessage] =
    msg.toLowerCase match {
      case "start"    => Some(Start())
      case "stop"     => Some(Stop())
      case "pause"    => Some(Pause())
      case "continue" => Some(Continue())
      case "remove"   => Some(Remove())
      case _          => None
    }

  def sendMsg(botId: Long, msg: String): Boolean =
    stringMsgToCommand(msg) match {
      case Some(actionMsg) =>
        sendMsg(botId, actionMsg)
      case _ =>
        log.error("Couldn't determine control message for string: " + msg)
        false
    }
}