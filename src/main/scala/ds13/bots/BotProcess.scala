package ds13.bots

import ds13.logger.TraitJSON
import ds13.logger.LoggerSupported

trait TraitBotProcess[+R, +C] extends TraitJSON with LoggerSupported {

  val id: Long

  var isFinishedFlag = false

  def getImmutableResult: Option[R] = None

  def isFinished: Boolean = isFinishedFlag

  def step {}

  def configure[C](config: C) {}

}

object BotProcessStates {

  val STATE_IDLE = 0

  val STATE_IN_PROCESS = STATE_IDLE + 1

  val STATE_FORM_RESULTS = STATE_IN_PROCESS + 1

  val STATE_CONFIGURE = STATE_FORM_RESULTS + 1

}

trait TraitBotProcessWithPause[+R, +C] extends TraitBotProcess[R, C] {

  def pause: Long = 1000

}
