package ds13.bots

trait StateMachineBotProcess[+R, +C] extends TraitBotProcessWithPause[R, C] {

  var state = BotProcessStates.STATE_IDLE

  def setState(state: Int) = this.state = state

  override def pause: Long = {
    val pauseTime = getPause
    debug("Time for pause: " + pauseTime)
    flushChanges
    pauseTime
  }

  def getPause: Long

  def flushChanges: Unit = {}

}
