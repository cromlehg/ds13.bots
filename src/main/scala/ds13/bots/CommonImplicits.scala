package ds13.bots

import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext

object CommmonImplicits {
  /*
    * TODO: replace with 
    * implicit val appContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10000))  
    * Now it have some problems in compilation like this: 
    * 
    * [error] app/controllers/AppImplicits.scala:9: value newWorkStealingPool is not a member of object java.util.concurrent.Executors
    * [error]   implicit val appContext = ExecutionContext.fromExecutor(Executors.newWorkStealingPool(100))
    * 
    */
  implicit val appContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(1000))

}