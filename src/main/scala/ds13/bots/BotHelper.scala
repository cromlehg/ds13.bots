package ds13.bots

import CommmonImplicits.appContext
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.ExecutionContext
import ds13.logger.LoggerSupported
import ds13.logger.Logger

trait TraitBotHelper[+R, +C, F <: TraitBotConfig, P <: TraitBotProcess[R, C]] extends LoggerSupported {

  val config: F

  var ref: Option[ActorRef] = None

  def createActor(id: Long): Option[Props] =
    createBotProcess(config, id: Long) map (botProcess => Props(new Bot[R, C](botProcess)))

  protected def createBotProcess(config: F, id: Long): Option[P]

  def status: Int =
    ref match {
      case Some(actorRef) =>
        implicit val timeout = Timeout(10 seconds)
        val future = (actorRef ? GetStatus())
        future onComplete {
          case Success(result) => result
          case Failure(error)  => BotStates.STATE_UNKNOWN
        }
        val result = Await result (future, timeout.duration)
        result.asInstanceOf[Int]
      case _ => BotStates.STATE_UNKNOWN
    }

}

abstract class AbstractBotHelper[+R, +C, F <: TraitBotConfig, P <: TraitBotProcess[R, C]](override val logger: Logger, override val config: F) extends TraitBotHelper[R, C, F, P]