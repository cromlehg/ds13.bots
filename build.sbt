name := "bots"

organization := "ds13"

version := "0.1"

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies ++= Seq(
  "ds13" % "logger_2.11" % "0.1" changing(),
  "org.scalatest" % "scalatest_2.11" % "3.0.0" % "test",
  "com.typesafe.akka" % "akka-actor_2.11" % "2.4.14",
  "org.json" % "json" % "20160810"
)

isSnapshot := true

val resolver = Resolver.ssh("DS13_Publish", "maven.siamway.ru", "/var/www/vhosts/maven.siamway.ru/httpdocs") withPermissions ("0644")

publishTo := Some(resolver as ("maven"))

publishArtifact in (packageSrc) := true

publishArtifact in (packageDoc) := true
